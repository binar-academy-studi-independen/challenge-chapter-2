class MataPelajaran {
  constructor(nama, kkm) {
    this.nama = nama;
    this.kkm = kkm;
  }
  getKkm() {
    return this.kkm;
  }
  getNama() {
    return this.nama;
  }
  setKkm(kkm) {
    this.kkm = kkm;
  }
  setNama(nama) {
    this.nama = nama;
  }
}

module.exports = { MataPelajaran };
