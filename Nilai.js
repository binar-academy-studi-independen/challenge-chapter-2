const { MataPelajaran } = require("./MataPelajaran.js");
class Nilai extends MataPelajaran {
  constructor(nama, kkm, nilai) {
    super(nama, kkm);
    this.nilai = nilai;
  }
  getNilai() {
    return this.nilai;
  }
  setNilai(nilai) {
    this.nilai = nilai;
  }

  getNilaiRatarata() {
    let total = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      total += this.nilai[i];
    }
    return (total / this.nilai.length).toFixed(2);
  }
  getNilaiTertinggi() {
    let max = this.nilai[0];
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] > max) max = this.nilai[i];
    }
    return max;
  }
  getNilaiTerendah() {
    let min = this.nilai[0];
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] < min) min = this.nilai[i];
    }
    return min;
  }

  getJumlahSiswaLulus() {
    let lulus = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] >= this.kkm) lulus++;
    }
    return lulus;
  }

  getJumlahSiswaTidakLulus() {
    let tidakLulus = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      if (this.nilai[i] < this.kkm) tidakLulus++;
    }
    return tidakLulus;
  }

  getUrutanNilai() {
    let temp = 0;
    for (let i = 0; i < this.nilai.length; i++) {
      for (let j = i + 1; j < this.nilai.length; j++) {
        if (this.nilai[i] > this.nilai[j]) {
          temp = this.nilai[i];
          this.nilai[i] = this.nilai[j];
          this.nilai[j] = temp;
        }
      }
    }
    return this.nilai;
  }
}

module.exports = { Nilai };
