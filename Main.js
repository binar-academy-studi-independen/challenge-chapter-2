const { Nilai } = require("./Nilai");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

let nilai = new Nilai("Matematika");
let arr = [];
nilai.setKkm(75);

rl.setPrompt(`inputkan nilai(enter) dan ketik "q" jika sudah selesai : `);
rl.prompt();
rl.on("line", (answer) => {
  if (answer != "q") {
    arr.push(Number(answer));
    rl.prompt();
    nilai.setNilai(arr);
  } else {
    rl.close();
  }
});

rl.on("close", () => {
  console.log(`Selesai memasukan nilai, ${nilai.getNama()} :
  Nilang Tertinggi : ${nilai.getNilaiTertinggi()}
  Nilai Terendah : ${nilai.getNilaiTerendah()}
  Nilai Rata - rata : ${nilai.getNilaiRatarata()}
  Urutan nilai dari terendah ke tertinggi :
  ${nilai.getUrutanNilai()}
  Jumlah Siswa Lulus / Tidak lulus :
  Lulus : ${nilai.getJumlahSiswaLulus()}
  Tidak lulus : ${nilai.getJumlahSiswaTidakLulus()}`);
  rl.close();
});
