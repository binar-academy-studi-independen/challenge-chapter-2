const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const arr = [];

const main = () => {
  rl.setPrompt(`inputkan nilai(enter) dan ketik "q" jika sudah selesai `);
  rl.prompt();
  rl.on("line", (answer) => {
    if (answer != "q") {
      arr.push(Number(answer));
      rl.prompt();
    } else {
      rl.close();
    }
  });

  rl.on("close", () => {
    console.log(`Selesai memasukan nilai, cari outputnya :
    Nilang Tertinggi : ${nilaiTertinggi(arr)}
    Nilai Terendah : ${nilaiTerendah(arr)}
    Nilai Rata - rata : ${nilaiRatarata(arr)}
    Urutan nilai dari terendah ke tertinggi :
    ${urutanNilai(arr)}
    Jumlah Siswa Lulus / Tidak lulus :
    Lulus : ${jumlahLulus(arr)}
    Tidak lulus : ${jumlahTidakLulus(arr)}`);
    rl.close();
  });
};

const nilaiTertinggi = (arr) => {
  let max = arr[0];
  for (i = 0; i < arr.length; i++) {
    if (arr[i] > max) max = arr[i];
  }
  return max;
};

const nilaiTerendah = (arr) => {
  let min = arr[0];
  for (i = 0; i < arr.length; i++) {
    if (arr[i] < min) min = arr[i];
  }
  return min;
};

const nilaiRatarata = (arr) => {
  let total = 0;
  for (let i = 0; i < arr.length; i++) {
    total += arr[i];
  }
  return (total / arr.length).toFixed(2);
};

const urutanNilai = (arr) => {
  let temp = 0;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
};

const jumlahLulus = (arr) => {
  let total = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] >= 75) {
      total.push(arr[i]);
    }
  }
  return total.length;
};

const jumlahTidakLulus = (arr) => {
  let total = [];
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] < 75) {
      total.push(arr[i]);
    }
  }
  return total.length;
};

main();
